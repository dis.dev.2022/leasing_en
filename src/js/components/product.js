import {getDigFormat} from "./functions.js"; // Полезные функции

export default () => {
    document.addEventListener('click', Checkout);

    function Checkout(event){
        if (event.target.closest('[data-product]')) {
            const container = event.target.closest('[data-product]');
            const productTerm = container.querySelectorAll('[data-term]');
            const productMileage = container.querySelectorAll('[data-mileage]');
            const productColor = container.querySelectorAll('[data-color]');
            const inputTerm = container.querySelector('input[name="product_term"]');
            const inputMileage = container.querySelector('input[name="product_mileage"]');
            const inputColor = container.querySelector('input[name="product_color"]');

            function PriceTotal() {
                let priceTerm = parseInt(container.querySelector('[data-term].active').dataset.term);
                let priceMileage = parseInt(container.querySelector('[data-mileage].active').dataset.mileage);
                let res = priceTerm + priceMileage;
                container.querySelector('[data-product-price]').innerHTML = getDigFormat(res);
            }

            if (event.target.closest('[data-term]')) {
                let elem = event.target.closest('[data-term]');

                productTerm.forEach(el => {
                    el.classList.remove('active');
                });
                elem.classList.add('active');
                inputTerm.value = elem.innerHTML;
                inputTerm.dataset.term = elem.dataset.term;

                PriceTotal();
            }

            if (event.target.closest('[data-mileage]')) {
                let elem = event.target.closest('[data-mileage]');

                productMileage.forEach(el => {
                    el.classList.remove('active');
                });
                elem.classList.add('active');
                inputMileage.value = elem.querySelector('.data-slider__legend').innerHTML;
                inputMileage.dataset.price = elem.dataset.mileage;

                PriceTotal();
            }

            if (event.target.closest('[data-color]')) {
                let elem = event.target.closest('[data-color]');

                productColor.forEach(el => {
                    el.classList.remove('active');
                });
                elem.classList.add('active');
                inputColor.value= elem.dataset.color;
                event.target.closest('[data-color-group]').querySelector('[data-color-active]').innerHTML = elem.dataset.color;
            }
        }
    }
};
