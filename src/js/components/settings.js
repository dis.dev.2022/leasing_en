import iro from '@jaames/iro';

(() => {
    document.querySelector('[data-settings-toggle]').addEventListener('click', (e) => {
       document.querySelector('[data-settings]').classList.toggle('open');
       return false;
    });

})();


(() => {
    let hexInput = document.querySelector('[data-picker-input-primary]');
    let colorPickerPrimary = new iro.ColorPicker("[data-picker-primary]", {
        width: 160,
        color: '#009FDF',
        layoutDirection: 'horizontal',
    });
    colorPickerPrimary.on(["color:init", "color:change"], function(color){
        let colorPrimary = document.querySelectorAll('[data-color-primary]')
        colorPrimary.forEach(elem => {
            elem.style.backgroundColor = color.hexString;
        });
        hexInput.value = color.hexString;
    });

    hexInput.addEventListener('change', function() {
        colorPickerPrimary.color.hexString = this.value;
    });

})();

(() => {
    let hexInput = document.querySelector('[data-picker-input-secondary]');
    let colorPickerSecondary = new iro.ColorPicker("[data-picker-secondary]", {
        width: 160,
        color: '#0078C3',
        layoutDirection: 'horizontal',
    });
    colorPickerSecondary.on(["color:init", "color:change"], function(color){
        let colorPrimary = document.querySelectorAll('[data-color-secondary]')
        colorPrimary.forEach(elem => {
            elem.style.backgroundColor = color.hexString;
        });
        hexInput.value = color.hexString;
    });

    hexInput.addEventListener('change', function() {
        colorPickerSecondary.color.hexString = this.value;
    });

})();

document.querySelector('[data-logo-header-input]').addEventListener('change', function(){
    if (this.files[0]) {
        let reader = new FileReader();

        reader.addEventListener('load', function () {
            document.querySelector('[data-logo-header]').src = reader.result;
            console.log(reader.result);
        }, false);


        reader.readAsDataURL(this.files[0]);
    }
});

document.querySelector('[data-logo-footer-input]').addEventListener('change', function(){
    if (this.files[0]) {
        let reader = new FileReader();

        reader.addEventListener('load', function () {
            document.querySelector('[data-logo-footer]').src = reader.result;
            console.log(reader.result);
        }, false);


        reader.readAsDataURL(this.files[0]);
    }
});
